# "Multi-Player Bandits Revisited"

This repository contains the LaTeX code of a research poster written by [Lilian Besson](http://perso.crans.org/besson/), entitled "Multi-Player Bandits Revisited".

- A 1-page A0-sized PDF poster, [written in LaTeX (LaTeX 2e)](http://www.latex-project.org/), with [the Baposter class/style](http://www.brian-amberg.de/uni/poster).
- This poster will be presented at the [Workshop on MAB and Learning Algorithms](http://www.erim.eur.nl/e-code-erasmus-centre-for-optimization-of-digital-experiments/workshop-on-multi-armed-bandits-and-learning-algorithms/), 18th of May 2018, in Rotterdam, Netherlands.
- This poster will be presented at the [ADDI PhD Student Day](http://addi.asso.insa-rennes.fr/), 15th of June 2018, at [the IETR lab](http://www.ietr.fr) in Rennes, France.

## *Abstract*
Multi-player Multi-Armed Bandits (MAB) have been extensively studied in the literature, motivated by applications to Cognitive Radio systems. Driven by such applications as well, we motivate the introduction of several levels of feedback for multi-player MAB algorithms. Most existing work assume that sensing information is available to the algorithm. Under this assumption, we improve the state-of-the-art lower bound for the regret of any decentralized algorithms and introduce two algorithms, RandTopM and MCTopM, that are shown to empirically outperform existing algorithms. Moreover, we provide strong theoretical guarantees for these algorithms, including a notion of asymptotic optimality in terms of the number of selections of bad arms. We then introduce a promising heuristic, called Selfish, that can operate without sensing information, which is crucial for emerging applications to Internet of Things networks. We investigate the empirical performance of this algorithm and provide some first theoretical elements for the understanding of its behavior.

## *Key words*:
- Multi-Armed Bandits
- Decentralized algorithms;
- Reinforcement learning;
- Cognitive Radio;
- Opportunistic Spectrum Access.

---

## Links
- The PDF is available here: [Poster JdD  Lilian Besson  Multi-Player Bandits.en.pdf](https://bitbucket.org/lbesson/phd-student-day-ietr-2018/downloads/poster.pdf)
- The LaTeX source code is available here: [Poster JdD  Lilian Besson  Multi-Player Bandits.en.tex](Poster_JdD__Lilian_Besson__Multi-Player_Bandits.en.tex)

![Poster_JdD__Lilian_Besson__Multi-Player_Bandits.en.pdf as a PNG image](Poster_JdD__Lilian_Besson__Multi-Player_Bandits.en.png)


## Article
This poster is based on [this article](https://hal.inria.fr/hal-01629733/document), presented to the [ALT 2018](http://www.cs.cornell.edu/conferences/alt2018/accepted.html) conference on April 2018.

We presented using [these slides](https://perso.crans.org/besson/publis/slides/2018_04__Presentation_at_ALT_2018_conference/slides.pdf) ([also in 16:9](https://perso.crans.org/besson/publis/slides/2018_04__Presentation_at_ALT_2018_conference/slides_169.pdf)) were used to present the article at the conference.

- PDF : [BK__ALT_2018.pdf](https://hal.inria.fr/hal-01629733/document)
- HAL notice : [BK__ALT_2018](https://hal.inria.fr/hal-01629733/)
- BibTeX : [BK__ALT_2018.bib](https://hal.inria.fr/hal-01629733/bibtex)

---

## About
This poster was done for the [PhD Student Day](http://addi.asso.insa-rennes.fr/) 2018, at IETR.

### Copyright
©, 2018 [Lilian Besson](http://perso.crans.org/besson/) ([IETR](http://ietr.fr/), [CentraleSupélec](http://www.centralesupelec.fr/), [SCEE Team](http://www.rennes.supelec.fr/ren/rd/scee/)).

<center><a title="Our poster as a PNG image, or a A0 PDF" href="https://bitbucket.org/lbesson/phd-student-day-ietr-2018/downloads/poster.pdf"><img width="700px" src="Poster_JdD__Lilian_Besson__Multi-Player_Bandits.en.png" alt="Poster_JdD__Lilian_Besson__Multi-Player_Bandits.en.pdf"></a></center>

> [I (Lilian Besson)](http://perso.crans.org/besson/) have [started my PhD](http://perso.crans.org/besson/phd/) in October 2016, and this is a part of my **on going** research in 2018.


### :scroll: License ?
This project is publicly published, under the terms of the [MIT Licensed](https://lbesson.mit-license.org/) (file [LICENSE](LICENSE)).

[![Published](https://img.shields.io/badge/Published%3F-accepted-green.svg)](https://hal.inria.fr/hal-01629733)
[![Maintenance](https://img.shields.io/badge/Maintained%3F-yes-green.svg)](https://bitbucket.org/lbesson/phd-student-day-ietr-2018/commits/)
[![Ask Me Anything !](https://img.shields.io/badge/Ask%20me-anything-1abc9c.svg)](https://Bitbucket.org/lbesson/ama)
[![Analytics](https://ga-beacon.appspot.com/UA-38514290-17/bitbucket.org/scee_ietr/phd-student-day-ietr-2017-bonnefoi-and-besson/commits/README.md?pixel)](https://bitbucket.org/lbesson/phd-student-day-ietr-2018/commits/)

[![ForTheBadge uses-badges](http://ForTheBadge.com/images/badges/uses-badges.svg)](http://ForTheBadge.com)
[![ForTheBadge uses-git](http://ForTheBadge.com/images/badges/uses-git.svg)](https://Bitbucket.org/)

[![made-with-latex](https://img.shields.io/badge/Made%20with-LaTeX-1f425f.svg)](https://www.latex-project.org/)
[![ForTheBadge built-with-science](http://ForTheBadge.com/images/badges/built-with-science.svg)](https://Bitbucket.org/lbesson/)
